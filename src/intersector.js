const Intersector = {}

Intersector.castRay = function (origin, direction){
    let closestT = Infinity;
    let closestObject = null;

    for (const object of objects) {
        if (object.type === 'sphere') {
            // Intersection with a sphere
            const oc = {
                x: origin.x - object.center.x,
                y: origin.y - object.center.y,
                z: origin.z - object.center.z,
            };

            const a = direction.x * direction.x + direction.y * direction.y + direction.z * direction.z;
            const b = 2 * (oc.x * direction.x + oc.y * direction.y + oc.z * direction.z);
            const c = oc.x * oc.x + oc.y * oc.y + oc.z * oc.z - object.radius * object.radius;
            const discriminant = b * b - 4 * a * c;

            if (discriminant > 0) {
                const t1 = (-b - Math.sqrt(discriminant)) / (2 * a);
                const t2 = (-b + Math.sqrt(discriminant)) / (2 * a);

                if (t1 > 0 && t1 < closestT) {
                    closestT = t1;
                    closestObject = object;
                }

                if (t2 > 0 && t2 < closestT) {
                    closestT = t2;
                    closestObject = object;
                }
            }
        } else if (object.type === 'rectangularPrism') {
            // Intersection with a rectangular prism

            const t1 = (object.min.x - origin.x) / direction.x;
            const t2 = (object.max.x - origin.x) / direction.x;
            const t3 = (object.min.y - origin.y) / direction.y;
            const t4 = (object.max.y - origin.y) / direction.y;
            const t5 = (object.min.z - origin.z) / direction.z;
            const t6 = (object.max.z - origin.z) / direction.z;

            const tmin = Math.max(Math.max(Math.min(t1, t2), Math.min(t3, t4)), Math.min(t5, t6));
            const tmax = Math.min(Math.min(Math.max(t1, t2), Math.max(t3, t4)), Math.max(t5, t6));

            if (tmax >= tmin && tmax >= 0) {
                if (tmin > 0 && tmin < closestT) {
                    closestT = tmin;
                    closestObject = object;
                }
            }
        } else if (object.type === 'triangle') {
            // Intersection with a triangle
            const v0 = object.vertices[0];
            const v1 = object.vertices[1];
            const v2 = object.vertices[2];

            const e1 = { x: v1.x - v0.x, y: v1.y - v0.y, z: v1.z - v0.z };
            const e2 = { x: v2.x - v0.x, y: v2.y - v0.y, z: v2.z - v0.z };
            const h = LinearAlgebra.crossProduct(direction, e2);
            const a = LinearAlgebra.dotProduct(e1, h);

            if (a > -0.00001 && a < 0.00001) {
                continue;
            }

            const f = 1 / a;
            const s = { x: origin.x - v0.x, y: origin.y - v0.y, z: origin.z - v0.z };
            const u = f * LinearAlgebra.dotProduct(s, h);

            if (u < 0.0 || u > 1.0) {
                continue;
            }

            const q = LinearAlgebra.crossProduct(s, e1);
            const v = f * LinearAlgebra.dotProduct(direction, q);

            if (v < 0.0 || u + v > 1.0) {
                continue;
            }

            const t = f * LinearAlgebra.dotProduct(e2, q);

            if (t > 0.00001 && t < closestT) {
                closestT = t;
                closestObject = object;
            }
        }
    }

    if (closestObject) {
        const hitPoint = {
            x: origin.x + direction.x * closestT,
            y: origin.y + direction.y * closestT,
            z: origin.z + direction.z * closestT,
        };
        return { object: closestObject, hitPoint };
    }

    return null;
}
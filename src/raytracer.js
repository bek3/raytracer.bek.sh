
// Set up ray tracer elements
const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

// Figure out how big of a space we are drawing in
canvas.width = window.innerWidth;
canvas.height = window.innerHeight * 0.75;
const canvasWidth = canvas.width;
const canvasHeight = canvas.height;
const aspectRatio = canvasWidth / canvasHeight;

// Camera lens field of view
// const fov = Math.PI / 2;


// x = left/right (positive right)
// y = up/down (positive down)
// z = close/far (positive close)

let objects = [];

let lights = [];


function generateLights(){
    lights = [];

    if (Controls.lighting.cbLantern.checked) lights.push(Scene.presetLights.lantern);
    if (Controls.lighting.cbSunHigh.checked) lights.push(Scene.presetLights.sunOverhead);
    if (Controls.lighting.cbSunBehind.checked) lights.push(Scene.presetLights.sunBack);
    if (Controls.lighting.cbSunFront.checked) lights.push(Scene.presetLights.sunFront);
    if (Controls.lighting.cbSunLeft.checked) lights.push(Scene.presetLights.sunLeft);
    if (Controls.lighting.cbSunRight.checked) lights.push(Scene.presetLights.sunRight);

    console.log("Using lights:", lights)
}

function generateScene(){
    objects = [];

    const selectedSceneName = Controls.functions.getSelectedSceneName();
    console.log("Using scene", selectedSceneName)

    if (selectedSceneName === "Grassy Field (day)") {
        objects.push(Scene.presetObjects.grass);
        objects.push(Scene.presetObjects.sky);
    }else if (selectedSceneName === "Grassy Field (night)"){
        objects.push(Scene.presetObjects.grass);
        objects.push(Scene.presetObjects.space);
    }else if (selectedSceneName === "Space"){
        objects.push(Scene.presetObjects.space);
    }else{
        throw "Invalid scene name";
    }

    // Shapes
    objects.push(Scene.debugObjects.spheres.redSphere);
    objects.push(Scene.debugObjects.spheres.greenSphere);
    objects.push(Scene.debugObjects.spheres.cyanSphere);
    objects.push(Scene.debugObjects.spheres.blueSphere);
    objects.push(Scene.debugObjects.spheres.purpleSphere);
    objects.push(Scene.debugObjects.spheres.blackSphere);
    objects.push(Scene.debugObjects.spheres.whiteSphere);
    objects.push(Scene.debugObjects.spheres.pinkSphere);
    objects.push(Scene.debugObjects.spheres.yellowSphere);

    objects.push(Scene.debugObjects.rectangularPrisms.yellowRec);
    objects.push(Scene.debugObjects.rectangularPrisms.cyanRec);
    objects.push(Scene.debugObjects.rectangularPrisms.monolith);

    console.log("Using objects:", objects)
}


function render() {
    console.log("Working")

    generateLights();
    generateScene();

    const camPosition = Scene.functions.getSelectedCameraPosition();
    const fov = Controls.functions.getFovRadians();

    console.log("Beginning rendering...");

    let totalFrames = 0;
    for (let x = 0; x < canvasWidth; x++) {
        for (let y = 0; y < canvasHeight; y++) {
            const u = (2 * x - canvasWidth) / canvasWidth;
            const v = (2 * y - canvasHeight) / canvasHeight;

            const direction = {
                x: Math.tan(fov / 2) * aspectRatio * u,
                y: Math.tan(fov / 2) * v,
                z: -1,
            };

            const ray = Intersector.castRay(camPosition, direction);

            if (ray) {
                const { object, hitPoint } = ray;
                let normal;

                if (object.type === 'sphere') {
                    normal = LinearAlgebra.normalize({
                        x: hitPoint.x - object.center.x,
                        y: hitPoint.y - object.center.y,
                        z: hitPoint.z - object.center.z,
                    });
                } else if (object.type === 'rectangularPrism') {
                    // Calculate normal for rectangular prism based on hit face
                    const epsilon = 0.00001;

                    if (Math.abs(hitPoint.x - object.min.x) < epsilon) {
                        normal = { x: -1, y: 0, z: 0 };
                    } else if (Math.abs(hitPoint.x - object.max.x) < epsilon) {
                        normal = { x: 1, y: 0, z: 0 };
                    } else if (Math.abs(hitPoint.y - object.min.y) < epsilon) {
                        normal = { x: 0, y: -1, z: 0 };
                    } else if (Math.abs(hitPoint.y - object.max.y) < epsilon) {
                        normal = { x: 0, y: 1, z: 0 };
                    } else if (Math.abs(hitPoint.z - object.min.z) < epsilon) {
                        normal = { x: 0, y: 0, z: -1 };
                    } else if (Math.abs(hitPoint.z - object.max.z) < epsilon) {
                        normal = { x: 0, y: 0, z: 1 };
                    }
                }

                const directionToCamera = LinearAlgebra.normalize({
                    x: camPosition.x - hitPoint.x,
                    y: camPosition.y - hitPoint.y,
                    z: camPosition.z - hitPoint.z,
                });

                let pixelColor = []
                if (Controls.functions.getSelectedShaderName() === "Phong Shading"){
                    pixelColor = Shader.phongShading(hitPoint, normal, directionToCamera, object);
                }else{
                    pixelColor = Shader.NoShading(object);
                }

                // Draw the pixel on the canvas
                ctx.fillStyle = `rgb(${pixelColor[0]}, ${pixelColor[1]}, ${pixelColor[2]})`;
                ctx.fillRect(x, y, 1, 1);

                totalFrames++;
            }
        }
    }
    console.log("Finished")
}
const Controls = {
    scene: {
        selector: document.getElementById('sceneSelection')
    },

    lighting:{
        cbLantern: document.getElementById("lightLantern"),
        cbSunHigh: document.getElementById("lightSunUp"),
        cbSunBehind: document.getElementById("lightSunBehindCam"),
        cbSunFront: document.getElementById("lightSunBehindScene"),
        cbSunLeft: document.getElementById("lightSunLeft"),
        cbSunRight: document.getElementById("lightSunRight")
    },

    info: {
    },

    params: {
        fov: document.getElementById("inputFov"),
        shadeMethod: document.getElementById("shaderSelection")
    },

    functions: {
        getSelectedSceneName: function () {
            const selectedOpt = Controls.scene.selector.options[Controls.scene.selector.selectedIndex];
            return selectedOpt.text;
        },

        getSelectedShaderName: function (){
            const selectedOpt = Controls.params.shadeMethod.options[Controls.params.shadeMethod.selectedIndex];
            return selectedOpt.text;
        },

        getFovRadians: function (){
            const fovDeg = Controls.params.fov.value;
            return fovDeg * Math.PI / 180;
        }
    }
}
// x = left/right (positive right)
// y = up/down (positive down)
// z = close/far (positive close)

const Scene = {
    camera: {
        origin: { x: 0, y: -1, z: 5 }
    },

    // Position: X,Y,Z per coordinate system above
    // Color: R,G,B tuple
    presetLights: {
        lantern: { position: { x: 0, y: -1, z: 5 }, color: [0.5, 0.5, 0.5] },
        sunOverhead: { position: { x: 0, y: -600, z: 0 }, color: [1, 1, 1] },
        sunFront: { position: { x: 0, y: -600, z: -600 }, color: [1, 1, 1] },  // Frontlight sun
        sunBack: { position: { x: 0, y: -600, z: 600 }, color: [1, 1, 1] },  // Backlight sun
        sunRight: { position: { x: 400, y: -600, z: 200 }, color: [1, 1, 1] },
        sunLeft: { position: { x: -400, y: -600, z: 200 }, color: [1, 1, 1] }
    },

    // Type: these are all rectangular
    // Minimum: first corner X,Y,Z
    // Maximum: Opposite corner X,Y,Z
    // Color: R,G,B tuple
    // Ambient: How much ambient light an object receives without one of the light sources above
    // Diffuse: How intensely the object is illuminated by a light source
    // Specular: How glossy the object is
    // Shininess: Another component used to determine how glossy/reflective the object is. Used in conjunction with Specular in phong shading
    presetObjects: {
        grass: { type: 'rectangularPrism', min: { x: -1000000, y: 0, z: -1000000 }, max: { x: 1000000, y: 1, z: 1000000 }, color: [50, 255, 0], ambient: 0.4, diffuse: 0.3, specular: 0.2, shininess: 0.4 },
        sky: { type: 'rectangularPrism', min: { x: -10000000, y: -1000, z: -10000000 }, max: { x: 10000000, y: -999, z: 10000000 }, color: [0, 0, 255], ambient: 1, diffuse: 0.9, specular: 0.1, shininess: 30 },
        space: { type: 'rectangularPrism', min: { x: -1000000000000, y: -1000000000000, z: -10000000000 }, max: { x: 1000000000000, y: 1000000000000, z: -10000000001 }, color: [0, 0, 0], ambient: 0, diffuse: 0.5, specular: 0, shininess: 0 }
    },

    debugObjects: {
        spheres: {
            redSphere: { type: 'sphere', center: { x: -8, y: -5, z: -10 }, radius: 2, color: [255, 0, 0], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            greenSphere: { type: 'sphere', center: { x: 4, y: -8, z: -5 }, radius: 1, color: [0, 255, 0], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            cyanSphere: { type: 'sphere', center: { x: -4, y: -7, z: -5 }, radius: 1, color: [0, 128, 255], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            blueSphere: { type: 'sphere', center: { x: -30, y: -1, z: -2 }, radius: 1, color: [0, 0, 255], ambient: 0.2, diffuse: 0.5, specular: 0.5, shininess: 1 },
            purpleSphere: { type: 'sphere', center: { x: 20, y: -8, z: -6 }, radius: 8, color: [255, 0, 255], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            blackSphere: { type: 'sphere', center: { x: -6, y: -1, z: -1 }, radius: 1, color: [0, 0, 0], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            whiteSphere: { type: 'sphere', center: { x: -4, y: -1, z: -2 }, radius: 1, color: [255, 255, 255], ambient: 0.1, diffuse: 0.9, specular: 0.4, shininess: 30 },
            pinkSphere: { type: 'sphere', center: { x: -18, y: -6, z: -2 }, radius: 6, color: [255, 192, 200], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            yellowSphere: { type: 'sphere', center: { x: 0, y: -7, z: -9 }, radius: 1, color: [255, 255, 0], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 }
        },
        rectangularPrisms: {
            yellowRec: { type: 'rectangularPrism', min: { x: -6, y: -5, z: -4 }, max: { x: -4, y: -4, z: -3 }, color: [255, 255, 0], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            cyanRec: { type: 'rectangularPrism', min: { x: 3, y: -2, z: -1 }, max: { x: 5, y: 0, z: 2 }, color: [0, 255, 255], ambient: 0.1, diffuse: 0.7, specular: 0.2, shininess: 30 },
            monolith: { type: 'rectangularPrism', min: { x: -2, y: -5, z: -8.5 }, max: { x: 2, y: 0, z: -8 }, color: [24, 24, 24], ambient: 0.1, diffuse: 0.7, specular: 0.8, shininess: 80 }
        }
    },

    functions: {
        getSelectedCameraPosition: function () {
            // todo: add more options
            return Scene.camera.origin;
        }
    }
}

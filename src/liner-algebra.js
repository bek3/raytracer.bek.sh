const LinearAlgebra = {}

LinearAlgebra.normalize = function (vector) {
    const length = Math.sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
    return { x: vector.x / length, y: vector.y / length, z: vector.z / length };
}

LinearAlgebra.dotProduct = function (v1, v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

LinearAlgebra.crossProduct = function (v1, v2) {
    return {
        x: v1.y * v2.z - v1.z * v2.y,
        y: v1.z * v2.x - v1.x * v2.z,
        z: v1.x * v2.y - v1.y * v2.x,
    };
}
const Shader = {
    preferredType: null,

    init: function (shadeType){
        this.preferredType = shadeType;
    },
};

Shader.NoShading = function (object){
    return object.color;
}

Shader.phongShading = function (hitPoint, normal, directionToCamera, object) {
    const ambient = object.ambient;
    const diffuse = object.diffuse;
    const specular = object.specular;
    const shininess = object.shininess;

    let totalColor = [0, 0, 0];

    for (const light of lights) {
        const lightPosition = light.position;
        const lightColor = light.color;

        const directionToLight = LinearAlgebra.normalize({
            x: lightPosition.x - hitPoint.x,
            y: lightPosition.y - hitPoint.y,
            z: lightPosition.z - hitPoint.z,
        });

        // Ambient component
        const ambientColor = [ambient * object.color[0], ambient * object.color[1], ambient * object.color[2]];

        // Diffuse component
        const lambertian = Math.max(LinearAlgebra.dotProduct(normal, directionToLight), 0);
        const diffuseColor = [diffuse * lambertian * object.color[0], diffuse * lambertian * object.color[1], diffuse * lambertian * object.color[2]];

        // Specular component
        const reflectionDirection = LinearAlgebra.normalize({
            x: 2 * LinearAlgebra.dotProduct(directionToLight, normal) * normal.x - directionToLight.x,
            y: 2 * LinearAlgebra.dotProduct(directionToLight, normal) * normal.y - directionToLight.y,
            z: 2 * LinearAlgebra.dotProduct(directionToLight, normal) * normal.z - directionToLight.z,
        });
        const specularFactor = Math.pow(Math.max(LinearAlgebra.dotProduct(reflectionDirection, directionToCamera), 0), shininess);
        const specularColor = [specular * specularFactor * 255, specular * specularFactor * 255, specular * specularFactor * 255];

        // Calculate the total color for this light source
        const lightContribution = [
            ambientColor[0] + diffuseColor[0] + specularColor[0],
            ambientColor[1] + diffuseColor[1] + specularColor[1],
            ambientColor[2] + diffuseColor[2] + specularColor[2],
        ];

        // Accumulate the contribution from this light source
        totalColor[0] += lightColor[0] * lightContribution[0];
        totalColor[1] += lightColor[1] * lightContribution[1];
        totalColor[2] += lightColor[2] * lightContribution[2];
    }
    return totalColor;
}

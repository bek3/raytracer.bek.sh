# Brendan's Online Ray Tracer

This is a simple app that is meant to teach the basic principles behind ray-tracing. See it in action at
raytracer.bek.sh.

This tool is unlicensed and is completely free for reuse without restriction.
